import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import pages.MspDashboardPage;
import pages.SignupPage;
import pages.TermsPage;
import pages.ActivateFreelancerPage;
import pages.FreelancerCreatePage;
import pages.FreelancerListPage;
import utilities.BrowserFactory;
import utilities.MailHelper;
import utilities.PropertyHelper;

public class PhilipsMSPCreate {
	protected static WebDriver driver;
	protected static WebDriverWait wait;
	protected static LoginPage loginPage;
	protected static HomePage homePage;
	protected static SignupPage signupPage;
	protected static TermsPage termsPage;
	protected static FreelancerListPage freelancerListPage;
	protected static MspDashboardPage mspdashboardPage;
	protected static ActivateFreelancerPage activateFreelancerPage;
	protected static FreelancerCreatePage freelancerCreatePage;

	private static Boolean mspPage = false;
	protected static String registerUserName;
	protected static String registerPassWord;
	protected static String correctPassword;
	protected static String baseFreelancerUrl;
	protected static String registerwrongMailid1;
	protected static String registerwrongMailid2;
	protected static String wrongPhoneNo;
	protected static String wrongPassword;
	private static String currentUsername;
	private static String currentPassword;
	protected static String incorrectRate;
	protected static String wrongvatid1;
	protected static String wrongvatid2;
	protected static String vatId;
	protected static String imgSource;
	protected static String fileSource;
	protected static String browserDriverUrl;

	@BeforeClass
	@Parameters({ "browser", "isMSPPage" })
	public static void setup(@Optional("firefox") String browser, @Optional("false") String isMSPPage)
			throws Exception {
		mspPage = Boolean.parseBoolean(isMSPPage);
		correctPassword = PropertyHelper.GetProperty("correctPassword");
		registerUserName = PropertyHelper.GetProperty("registerMailId");
		registerPassWord = PropertyHelper.GetProperty("registerPassword");
		baseFreelancerUrl = PropertyHelper.GetProperty("baseFreelancerUrl");
		registerwrongMailid1 = PropertyHelper.GetProperty("registerwrongMailid1");
		registerwrongMailid2 = PropertyHelper.GetProperty("registerwrongMailid2");
		wrongPhoneNo = PropertyHelper.GetProperty("wrongPhoneNo");
		wrongPassword = PropertyHelper.GetProperty("wrongPassword");
		currentUsername = PropertyHelper.GetProperty("mailId");
		currentPassword = PropertyHelper.GetProperty("MSPpassword");
		incorrectRate = PropertyHelper.GetProperty("incorrectRate");
		wrongvatid1 = PropertyHelper.GetProperty("wrongvatid1");
		wrongvatid2 = PropertyHelper.GetProperty("wrongvatid2");
		vatId = PropertyHelper.GetProperty("vatid");
		imgSource = PropertyHelper.GetProperty("imgSource");
		fileSource = PropertyHelper.GetProperty("fileSource");
		browserDriverUrl = PropertyHelper.GetProperty(browser + "DriverUrl");
		driver = BrowserFactory.getWebDriver(browser, browserDriverUrl, mspPage);
		BrowserFactory.LoadWebPage("/", true);
	}

	@Test(priority = 201)
	public void TC_CreateFreelancer_002() {
		loginPage = new LoginPage(driver);
		loginPage.setEmail(currentUsername);
		loginPage.setPassword(currentPassword);
		loginPage.clickMspSubmit();
		mspdashboardPage = new MspDashboardPage(driver);
		mspdashboardPage.clickFreelancer();
		freelancerListPage = new FreelancerListPage(driver);
		freelancerListPage.clickCreate();
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.setFirstName("kash1223");
		Assert.assertEquals(freelancerCreatePage.getfirstNameError(), "Letters only");
	}

	@Test(priority = 202)
	public void TC_CreateFreelancer_003() {

		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.setLastName("kashme1233");
		Assert.assertEquals(freelancerCreatePage.getlastNameError(), "Letters only");

	}

	@Test(priority = 203)
	public void TC_CreateFreelancer_004() {

		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.setEmail(registerwrongMailid1);
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getemailError(), "Please enter a valid email address.");
	}

	@Test(priority = 204)
	public void TC_CreateFreelancer_005() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.setEmail(registerwrongMailid2);
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getemailError(), "Please enter a valid email address.");
	}

	@Test(priority = 205)
	public void TC_CreateFreelancer_006() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getbirthDaySingleError(), "This field is required.");

		Assert.assertEquals(freelancerCreatePage.getbirthMonthError(), "This field is required.");

		Assert.assertEquals(freelancerCreatePage.getbirthYearError(), "This field is required.");

	}

	@Test(priority = 206)
	public void TC_CreateFreelancer_007() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.setMobileNumber(wrongPhoneNo);
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getmobileError(), "Please enter only digits.");

	}

	@Test(priority = 207)
	public void TC_CreateFreelancer_008() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.setPhoneNumber(wrongPhoneNo);
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getphoneError(), "Please enter only digits.");
	}

	@Test(priority = 208)
	public void TC_CreateFreelancer_009() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getnationalityError(), "This field is required.");
	}

	@Test(priority = 209)
	public void TC_CreateFreelancer_010() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getcountryError(), "This field is required.");
	}

	@Test(priority = 210)
	public void TC_CreateFreelancer_011() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.gethomeAddressLine1Error(), "This field is required.");
	}

	@Test(priority = 211)
	public void TC_CreateFreelancer_012() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getStreetError(), "This field is required.");
	}

	@Test(priority = 212)
	public void TC_CreateFreelancer_013() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getCityError(), "This field is required.");
	}

	@Test(priority = 213)
	public void TC_CreateFreelancer_014() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getZipError(), "This field is required.");
	}

	@Test(priority = 214)
	public void TC_CreateFreelancer_015() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.setNonEuCitizen();
		Assert.assertEquals(freelancerCreatePage.verifyNonEUElements(), true);

	}

	@Test(priority = 215)
	public void TC_CreateFreelancer_016() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.sethourRateValue(incorrectRate);
		Assert.assertEquals(freelancerCreatePage.gethourRateError(), "Please enter only digits.");

	}

	@Test(priority = 216)
	public void TC_CreateFreelancer_017() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.choosediffbillingAddressOption();
		Assert.assertEquals(freelancerCreatePage.verifydiffbillingAddressElements(), true);
	}

	@Test(priority = 217)
	public void TC_CreateFreelancer_018() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.choosediffbillingAddressOption();
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getBillingCountryError(), "This field is required.");
	}

	@Test(priority = 218)
	public void TC_CreateFreelancer_019() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.choosediffbillingAddressOption();
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getBillingAddressLine1Error(), "This field is required.");
	}

	@Test(priority = 219)
	public void TC_CreateFreelancer_020() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.choosediffbillingAddressOption();
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getBillingStreetError(), "This field is required.");
	}

	@Test(priority = 220)
	public void TC_CreateFreelancer_021() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.choosediffbillingAddressOption();
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getBillingCityError(), "This field is required.");
	}

	@Test(priority = 221)
	public void TC_CreateFreelancer_022() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.choosediffbillingAddressOption();
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getBillingZipError(), "This field is required.");
	}

	@Test(priority = 222)
	public void TC_CreateFreelancer_023() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.inputValidData();
		freelancerCreatePage.setvatId(wrongvatid1);
		freelancerCreatePage.clickSubmit();
		Assert.assertEquals(freelancerCreatePage.getVatIDError(), "VAT is invalid");
	}

	// @Test(priority = 223)
	// public void TC_CreateFreelancer_024() {
	// freelancerCreatePage =new FreelancerCreatePage(driver);
	//
	// try {
	// freelancerCreatePage.inputValidData();
	// freelancerCreatePage.fileUpload(fileSource);
	//
	// } catch (InterruptedException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// Assert.assertEquals(freelancerCreatePage.getUploadFilesInfo(), "Attached
	// files will not be send to the client.");
	//
	// }
	// @Test(priority = 224)
	// public void TC_CreateFreelancer_025() {
	// freelancerCreatePage =new FreelancerCreatePage(driver);
	// try {
	// freelancerCreatePage.imageUpload(imgSource);
	// } catch (InterruptedException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// freelancerCreatePage.clickSubmit();
	//
	// }
	@Test(priority = 225)
	public void TC_CreateFreelancer_026() {
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.clickFreelancer();
		freelancerListPage = new FreelancerListPage(driver);
		freelancerListPage.clickCreate();
		freelancerCreatePage = new FreelancerCreatePage(driver);
		freelancerCreatePage.inputValidData();
		freelancerCreatePage.setvatId(vatId);
		// freelancerCreatePage.imageUpload(imgSource);
		freelancerCreatePage.clickSubmit();

	}

//	@Test(priority = 226)
//	public void TC_CreateFreelancer_027() throws Exception {
//		String activeBaseUrl = "http://elink.foobar.freelance-management.com";
//		freelancerCreatePage.verifyCreateFreelancer();
//		try {
//			String activationLink = MailHelper.GetUrl("Welcome to the Philips Freelance Platform", activeBaseUrl,
//					registerUserName, registerPassWord);
//			System.out.println(activationLink);
//			Assert.assertTrue(!activationLink.isEmpty());
//			BrowserFactory.LoadWebPage(activeBaseUrl + activationLink, false, true);
//			activateFreelancerPage = new ActivateFreelancerPage(driver);
//			Assert.assertEquals(driver.getTitle(), "Account Activated");
//			// activateFreelancerPage.clickModalOk();;
//
//		} catch (Exception E) {
//			throw E;
//		}
//	}
   
	
	
//	@Test(priority = 27)
//	public void TC_CreateFreelancer_028() throws Exception {
//	String activeBaseUrl = "http://elink.foobar.freelance-management.com";
//	freelancerCreatePage.verifyCreateFreelancer();
//		try {
//		String activationLink = MailHelper.GetUrl("Welcome to the Philips Freelance Platform", activeBaseUrl,
//					registerUserName, registerPassWord);
//			System.out.println(activationLink);
//			Assert.assertTrue(!activationLink.isEmpty());
//			BrowserFactory.LoadWebPage(activeBaseUrl + activationLink, false, true);
//			activateFreelancerPage = new ActivateFreelancerPage(driver);
//			Assert.assertEquals(driver.getTitle(), "Account Activated");
//			// activateFreelancerPage.clickModalOk();;
//
//		} catch (Exception E) {
//			throw E;
//		}
//	}
	@AfterClass
	public static void teardown() {
		driver.close();
		driver.quit();
	}

}
