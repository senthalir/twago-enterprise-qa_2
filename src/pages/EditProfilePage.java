package pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.BrowserFactory;
import utilities.PropertyHelper;

public class EditProfilePage {
	protected static WebDriver driver;
	protected static WebDriverWait wait;
	protected static String salutaion;
	protected static String firstName1;
	protected static String lastName1;
	protected static String birthDay1;
	protected static String birthMonth1;
	protected static String birthYear1;
	protected static String correctPhoneNo;
	protected static String correctPassword; 
	protected static String nationalityValue;
	protected static String countryValue;
	protected static String homeaddressline1Value;
	protected static String streetaddress;
	protected static String cityValue;
	protected static String zipCodeValue;
	protected static String hourValue;
	protected static String ratecurrency;
	protected static String ibanValue;
	protected static String bicValue;
	protected static String commerceNo;
	protected static String vatId;
	
	public EditProfilePage(WebDriver wd) {
		driver = wd;
		PageFactory.initElements(wd, this);
		wait = new WebDriverWait(wd, 60);
		salutaion = PropertyHelper.GetProperty("salutation");
		firstName1 = PropertyHelper.GetProperty("firstName");
		lastName1 = PropertyHelper.GetProperty("lastName");
		birthDay1 = PropertyHelper.GetProperty("birthDay");
		birthMonth1 = PropertyHelper.GetProperty("birthMonth");
		birthYear1 = PropertyHelper.GetProperty("birthYear");
		nationalityValue= PropertyHelper.GetProperty("nationality");
		correctPhoneNo = PropertyHelper.GetProperty("correctPhoneNo");
		countryValue=PropertyHelper.GetProperty("country");
		homeaddressline1Value =PropertyHelper.GetProperty("homeaddressline1");
		streetaddress =PropertyHelper.GetProperty("streetaddress");
		cityValue=PropertyHelper.GetProperty("city");
		zipCodeValue=PropertyHelper.GetProperty("zipcode");
		hourValue=PropertyHelper.GetProperty("hourvalue");
		ratecurrency=PropertyHelper.GetProperty("ratecurrency");
		ibanValue=PropertyHelper.GetProperty("iban");
		bicValue=PropertyHelper.GetProperty("bic");
		commerceNo=PropertyHelper.GetProperty("commerceno");
		vatId = PropertyHelper.GetProperty("vatid");
		
	}

	@FindBy(how = How.ID, using = "salutation")
	@CacheLookup
	WebElement salutation;

	@FindBy(how = How.CSS, using = "input[value='m'][type=radio] ~ .radio-text")
	@CacheLookup
	WebElement maleGenderOption;

	@FindBy(how = How.CSS, using = "input[value='f'][type=radio] ~ .radio-text")
	@CacheLookup
	WebElement femaleGenderOption;

	@FindBy(how = How.CSS, using = "#firstName")
	@CacheLookup
	WebElement firstName;

	@FindBy(how = How.ID, using = "lastName")
	@CacheLookup
	WebElement lastName;

	@FindBy(how = How.ID, using = "email")
	@CacheLookup
	WebElement email;
	
	@FindBy(how = How.ID, using = "nationality")
	@CacheLookup
	WebElement nationality;
	
	@FindBy(how = How.ID, using = "birthMonth")
	@CacheLookup
	WebElement birthMonth;

	@FindBy(how = How.ID, using = "birthDaySingle")
	@CacheLookup
	WebElement birthDay;

	@FindBy(how = How.ID, using = "birthYear")
	@CacheLookup
	WebElement birthYear;

	@FindBy(how = How.ID, using = "mobile")
	@CacheLookup
	WebElement mobileNo;

	@FindBy(how = How.ID, using = "phone")
	@CacheLookup
	WebElement phoneNo;

	@FindBy(how = How.ID, using = "country")
	@CacheLookup
	WebElement country;

	@FindBy(how = How.ID, using = "homeAddressLine1")
	@CacheLookup
	WebElement homeAddressLine1;
	
	@FindBy(how = How.ID, using = "homeAddressLine2")
	@CacheLookup
	WebElement homeAddressLine2;
	
	@FindBy(how = How.ID, using = "street")
	@CacheLookup
	WebElement street;
	
	@FindBy(how = How.ID, using = "city")
	@CacheLookup
	WebElement city;
	
	@FindBy(how = How.ID, using = "zip")
	@CacheLookup
	WebElement zipCode;
	
	@FindBy(how = How.CSS, using = "#nonEuCitizenCheckbox ~ .radio-text")
	@CacheLookup
	WebElement nonEuCitizenCheckbox;
	
	@FindBy(how = How.CSS, using = "#euCitizenCheckbox ~ .radio-text")
	@CacheLookup
	WebElement euCitizenCheckbox;
	
	@FindBy(how = How.ID, using = "workPermitEnd")
	@CacheLookup
	WebElement workPermitEnd;
	
	@FindBy(how = How.ID, using = "visaEnd")
	@CacheLookup
	WebElement visaEnd;
	
	@FindBy(how = How.ID, using = "hourRateValue")
	@CacheLookup
	WebElement hourRateValue;
	
	@FindBy(how = How.ID, using = "hourRateCurrency")
	@CacheLookup
	WebElement hourRateCurrency;
	
	@FindBy(how = How.CSS, using=".js-address-input ~ .radio-text")
	@CacheLookup
	WebElement homeAddressRadio;
	
	@FindBy(how = How.CSS, using="#checkBillingAddress ~ .radio-text")
	@CacheLookup
	WebElement billingAddressRadio;
	
	@FindBy(how = How.ID, using = "billingCountry")
	@CacheLookup
	WebElement billingCountry;

	@FindBy(how = How.ID, using = "billingAddressLine1")
	@CacheLookup
	WebElement billingAddressLine1;
	
	@FindBy(how = How.ID, using = "billingAddressLine2")
	@CacheLookup
	WebElement billingAddressLine2;
	
	@FindBy(how = How.ID, using = "billingStreet")
	@CacheLookup
	WebElement billingStreet;
	
	@FindBy(how = How.ID, using = "billingCity")
	@CacheLookup
	WebElement billingCity;
	
	@FindBy(how = How.ID, using = "billingZip")
	@CacheLookup
	WebElement billingZip;
	
	@FindBy(how = How.ID, using = "iban")
	@CacheLookup
	WebElement iban;
	
	@FindBy(how = How.ID, using = "bic")
	@CacheLookup
	WebElement bic;
	
	@FindBy(how = How.ID, using = "vatNumber")
	@CacheLookup
	WebElement vatID;

	@FindBy(how = How.ID, using = "chamberOfCommerceNumber")
	@CacheLookup
	WebElement chamberOfCommerceNumber;
	
	@FindBy(how = How.ID, using = "fileUploadAdd")
	@CacheLookup
	WebElement fileUploadAdd;
	
	@FindBy(how = How.ID, using = "createFreelancerSubmit")
	@CacheLookup
	WebElement saveButton;
	
	
	@FindBy(how = How.ID, using = "addProfileImage")
	@CacheLookup
	WebElement addProfileImage;
	
	public String getbirthDate() {
		Select selectBirthdate = new Select(birthDay);
		return selectBirthdate.getFirstSelectedOption().getText().trim();
	}
public String getbirthMonth() {
	Select selectBirthdate = new Select(birthMonth);
	return selectBirthdate.getFirstSelectedOption().getText().trim();
	}
public String getbirthYear() {
	Select selectBirthdate = new Select(birthYear);
	return selectBirthdate.getFirstSelectedOption().getText().trim();
}
	
	public String getfirstNameError() {
		WebElement firstNameError = driver.findElement(By.id("firstName-error"));
		return firstNameError.getText().trim();
	}

	public String getlastNameError() {
		WebElement lastNameError = driver.findElement(By.id("lastName-error"));
		return lastNameError.getText().trim();
	}

	public String getemailError() {
		WebElement emailError = driver.findElement(By.id("email-error"));
		return emailError.getText().trim();
	}
	
	public String getnationalityError() {
		WebElement nationalityError = driver.findElement(By.id("nationality-error"));
		return nationalityError.getText().trim();
	}
	
	public String getbirthMonthError() {
		WebElement birthMonthError = driver.findElement(By.id("birthMonth-error"));
		return birthMonthError.getText().trim();
	}

	public String getbirthDaySingleError() {
		WebElement birthDaySingleError = driver.findElement(By.id("birthDaySingle-error"));
		return birthDaySingleError.getText().trim();
	}

	public String getbirthYearError() {
		WebElement birthYearError = driver.findElement(By.id("birthYear-error"));
		return birthYearError.getText().trim();
	}

	public String getmobileError() {
		WebElement mobileError = driver.findElement(By.id("mobile-error"));
		return mobileError.getText().trim();
	}

	public String getphoneError() {
		WebElement phoneError = driver.findElement(By.id("phone-error"));
		return phoneError.getText().trim();
	}

	public String getcountryError() {
		WebElement countryError = driver.findElement(By.id("country-error"));
		return countryError.getText().trim();
	}

	public String gethomeAddressLine1Error() {
		WebElement homeAddressLine1Error = driver.findElement(By.id("homeAddressLine1-error"));
		return homeAddressLine1Error.getText().trim();
	}
	public String getStreetError() {
		WebElement streetError = driver.findElement(By.id("street-error"));
		return streetError.getText().trim();
	}
	public String getCityError() {
		WebElement cityError = driver.findElement(By.id("city-error"));
		return cityError.getText().trim();
	}
	public String getZipError() {
		WebElement zipError = driver.findElement(By.id("zip-error"));
		return zipError.getText().trim();
	}
	public String getworkPermitEndError() {
		WebElement workPermitEndError = driver.findElement(By.id("workPermitEnd-error"));
		return workPermitEndError.getText().trim();
	}
	public String getVisaEndError() {
		WebElement visaEndError = driver.findElement(By.id("visaEnd-error"));
		return visaEndError.getText().trim();
	}
	
	public String gethourRateError() {
		WebElement hourRateError = driver.findElement(By.id("hourRateValue-error"));
		return hourRateError.getText().trim();
	}
	public String getBillingCountryError() {
		WebElement billingCountryError = driver.findElement(By.id("billingCountry-error"));
		return billingCountryError.getText().trim();
	}

	public String getBillingAddressLine1Error() {
		WebElement billingAddressLine1Error = driver.findElement(By.id("billingAddressLine1-error"));
		return billingAddressLine1Error.getText().trim();
	}
	public String getBillingStreetError() {
		WebElement billingStreetError = driver.findElement(By.id("billingStreet-error"));
		return billingStreetError.getText().trim();
	}
	public String getBillingCityError() {
		WebElement billingCityError = driver.findElement(By.id("billingCity-error"));
		return billingCityError.getText().trim();
	}
	public String getBillingZipError() {
		WebElement billingZipError = driver.findElement(By.id("billingZip-error"));
		return billingZipError.getText().trim();
	}
	public String getUploadFilesInfo() {
		WebElement uploadfilesInfo = driver.findElement(By.className("upload-files-info"));
		 wait.until(ExpectedConditions.attributeContains(By.id("serverErrorBox"), "class", "is-active"));
		return uploadfilesInfo.getText().trim();
	}
	public String getVatIDError(){
		 wait.until(ExpectedConditions.attributeContains(By.id("serverErrorBox"), "class", "is-active"));
		WebElement vatIdError = driver.findElement(By.id("serverErrorBox"));
		//System.out.println(vatIdError.getText());
		return vatIdError.getText().trim();
	}
	public String getSuccessMessage(){
		 wait.until(ExpectedConditions.attributeContains(By.id("serverSuccess"), "class", "is-active"));
		WebElement successMessage = driver.findElement(By.id("serverSuccess"));
		//System.out.println(vatIdError.getText());
		return successMessage.getText().trim();
	}
	public void setSalutation(String gender) {
		
		Select selectSalute = new Select(salutation);
		selectSalute.selectByVisibleText(gender);
		
	}

	public void selectFemaleGender() {
		femaleGenderOption.click();
	}

	public void selectMaleGender() {
		maleGenderOption.click();
	}

	public void setFirstName(String firstname) {
		
		firstName.clear();
		firstName.sendKeys(firstname);
		firstName.sendKeys(Keys.TAB);

	}
	public void clearFirstName()
	{
		firstName.clear();
	}

	public void setLastName(String lastname) {
		lastName.clear();
		lastName.sendKeys(lastname);
		lastName.sendKeys(Keys.TAB);
	}
	public void clearLastName()
	{
		lastName.clear();
	}
	public void setEmail(String email1) {
		email.clear();
		email.sendKeys(email1);

	}
	public void clearEmail()
	{
		email.clear();
	}
	public void setNationality(String nationality1) {
		
		Select selectnationality =new Select(nationality);
		selectnationality.selectByVisibleText(nationality1);

	}

	
	public void setBirthMonth(String month) {
		Select selectmonth = new Select(birthMonth);
		selectmonth.selectByVisibleText(month);
	}

	public void setBirthDay(String day) {
		Select selectday = new Select(birthDay);
		selectday.selectByVisibleText(day);
	}

	public void setBirthYear(String year) {
		Select selectyear = new Select(birthYear);
		selectyear.selectByVisibleText(year);
	}

	public void setMobileNumber(String mno) {
		mobileNo.clear();
		mobileNo.sendKeys(mno);
	}

	public void setPhoneNumber(String pno) {
		phoneNo.clear();
		phoneNo.sendKeys(pno);
	}


	public void setCountry(String country1) {
		Select selectCountry =new Select(country);
		selectCountry.selectByVisibleText(country1);
	}
	
	public void setHomeAddressLine1(String homeAddressLine) {
		homeAddressLine1.clear();
		homeAddressLine1.sendKeys(homeAddressLine);
	}
	
	public void setStreet(String street1) {
		street.clear();
		street.sendKeys(street1);
	}

	public void setCity(String city1) {
		city.clear();
		city.sendKeys(city1);
	}
	public void setZip(String Zip1) {
		zipCode.clear();
		zipCode.sendKeys(Zip1);
	}
	public void setEuCitizen() {
		
		BrowserFactory.SelectRadioButtonOrCheckbox(euCitizenCheckbox);
	}
	
	public void setNonEuCitizen() {
		
		BrowserFactory.SelectRadioButtonOrCheckbox(nonEuCitizenCheckbox);
	}
	public void setworkPermitEnd(String workPermitEnd1) {
		  JavascriptExecutor js = (JavascriptExecutor)driver;
		  js.executeScript("workPermitEnd.value='2018-01-01'");
		
	}
	public void setvisaEnd(String visaEnd1) {
		 JavascriptExecutor js = (JavascriptExecutor)driver;
		  js.executeScript("visaEnd.value='2018-01-01'");
	}
	
	public void sethourRateValue(String rate) {
		hourRateValue.clear();
		hourRateValue.sendKeys(rate);
	}
	public void choosediffbillingAddressOption()
	{
		BrowserFactory.SelectRadioButtonOrCheckbox(billingAddressRadio);
	}
	public void choosehomeaddressasbillingAddressOption()
	{
		BrowserFactory.SelectRadioButtonOrCheckbox(homeAddressRadio);
	}
	public void setBillingCountry(String country1) {
		Select selectCountry1 =new Select(billingCountry);
		selectCountry1.selectByVisibleText(country1);
	}
	
	public void setBillingAddressLine1(String billingAddressLine) {
		billingAddressLine1.clear();
		billingAddressLine1.sendKeys(billingAddressLine);
	}
	
	public void setBillingStreet(String street1) {
		billingStreet.clear();
		billingStreet.sendKeys(street1);
	}

	public void setBillingCity(String city1) {
		billingCity.clear();
		billingCity.sendKeys(city1);
	}
	public void setBillingZip(String Zip1) {
		billingZip.clear();
		billingZip.sendKeys(Zip1);
	}
	public void setIbanValue(String ibanvalue) {
		iban.clear();
		iban.sendKeys(ibanvalue);
	}
	public void setBicValue(String bicvalue) {
		bic.clear();
		bic.sendKeys(bicvalue);
	}
	public void setvatId(String vatid) {
		vatID.clear();
		vatID.sendKeys(vatid);
	}
	public void setcommerceNo(String commerceno) {
		chamberOfCommerceNumber.clear();
		chamberOfCommerceNumber.sendKeys(commerceno);
	}
	public void fileUpload(String fileLocation)
	{
		
		fileUploadAdd.click();
		fileUploadAdd.sendKeys(fileLocation);
		
		
	}
	public void imageUpload(String imageLocation)
	{
		addProfileImage.click();
		wait.until(ExpectedConditions.alertIsPresent());

		// switch to the file upload window
		 Alert alert = driver.switchTo().alert();

		// enter the filename
		alert.sendKeys(imageLocation);
		
		//driver.switchTo().frame(0).switchTo().activeElement().sendKeys(imageLocation);

		// hit enter
		Robot r;
		try {
			r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// switch back
		driver.switchTo().activeElement();
//		driver.switchTo().alert().sendKeys(imageLocation);
//		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}
	public void clickSubmit()
	{
		saveButton.click();
	}
	public boolean verifyNonEUElements()
	{
		if((workPermitEnd)!= null){
			return true;
		}else{
		return false;
		}
	}
	public boolean verifydiffbillingAddressElements()
	{
		if((billingAddressLine1)!= null){
			return true;
		}else{
		return false;
		}
	}
	public void inputValidData() {
		this.setSalutation(salutaion);
		this.selectFemaleGender();
		this.setFirstName(firstName1);
		this.setLastName(lastName1);
        this.setBirthDay(birthDay1);
        this.setBirthMonth(birthMonth1);
        this.setBirthYear(birthYear1);
        this.setNationality(nationalityValue);
        long rno = Math.round(Math.random()*100);
      	this.setEmail("senthalir.kashme+" +rno+ "@twago.com");
        this.setPhoneNumber(correctPhoneNo);
        this.setMobileNumber(correctPhoneNo);
       this.setCountry(countryValue);
       this.setHomeAddressLine1(homeaddressline1Value);
       this.setStreet(streetaddress);
       this.setCity(cityValue);
       this.setZip(zipCodeValue);
       this.setEuCitizen();
       this.sethourRateValue(hourValue);
       this.choosehomeaddressasbillingAddressOption();
       this.setIbanValue(ibanValue);
       this.setBicValue(bicValue);
       this.setcommerceNo(commerceNo);
       this.setvatId(vatId);
	}

	
}
