package pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.BrowserFactory;
import utilities.PropertyHelper;

public class EditCVPage {
	protected static WebDriver driver;
	protected static WebDriverWait wait;
	protected static String salutaion;
	protected static String firstName1;
	protected static String lastName1;
	protected static String birthDay1;
	protected static String birthMonth1;
	protected static String birthYear1;
	protected static String correctPhoneNo;
	protected static String correctPassword; 
	protected static String nationalityValue;
	protected static String countryValue;
	protected static String homeaddressline1Value;
	protected static String streetaddress;
	protected static String cityValue;
	protected static String zipCodeValue;
	protected static String hourValue;
	protected static String ratecurrency;
	protected static String ibanValue;
	protected static String bicValue;
	protected static String commerceNo;
	protected static String vatId;
	
	public EditCVPage(WebDriver wd) {
		driver = wd;
		PageFactory.initElements(wd, this);
		wait = new WebDriverWait(wd, 60);
		salutaion = PropertyHelper.GetProperty("salutation");
		firstName1 = PropertyHelper.GetProperty("firstName");
		lastName1 = PropertyHelper.GetProperty("lastName");
		birthDay1 = PropertyHelper.GetProperty("birthDay");
		birthMonth1 = PropertyHelper.GetProperty("birthMonth");
		birthYear1 = PropertyHelper.GetProperty("birthYear");
		nationalityValue= PropertyHelper.GetProperty("nationality");
		correctPhoneNo = PropertyHelper.GetProperty("correctPhoneNo");
		countryValue=PropertyHelper.GetProperty("country");
		homeaddressline1Value =PropertyHelper.GetProperty("homeaddressline1");
		streetaddress =PropertyHelper.GetProperty("streetaddress");
		cityValue=PropertyHelper.GetProperty("city");
		zipCodeValue=PropertyHelper.GetProperty("zipcode");
		hourValue=PropertyHelper.GetProperty("hourvalue");
		ratecurrency=PropertyHelper.GetProperty("ratecurrency");
		ibanValue=PropertyHelper.GetProperty("iban");
		bicValue=PropertyHelper.GetProperty("bic");
		commerceNo=PropertyHelper.GetProperty("commerceno");
		vatId = PropertyHelper.GetProperty("vatid");
		
	}

	@FindBy(how = How.CLASS_NAME, using = "resume-name")
	@CacheLookup
	WebElement resumeName;
	
	@FindBy(how = How.CSS, using = ".resume-personal-details .resume-details-row:nth-of-type(1) .resume-details-value")
	@CacheLookup
	WebElement resumeLocation;
	
	@FindBy(how = How.CSS, using = ".resume-personal-details .resume-details-row:nth-of-type(2) .resume-details-value")
	@CacheLookup
	WebElement resumeNationality;
	
	@FindBy(how = How.CSS, using = ".resume-personal-details .resume-details-row:nth-of-type(3) .resume-details-value")
	@CacheLookup
	WebElement resumeAvailability;
	
	@FindBy(how = How.CLASS_NAME, using = "resume-email")
	@CacheLookup
	WebElement resumeEmail;
	
	@FindBy(how = How.CLASS_NAME, using = "resume-phone")
	@CacheLookup
	WebElement resumePhone;
	
	@FindBy(how = How.CSS, using = " .js-availability-holder .freelancer-profile-availability")
	@CacheLookup
	WebElement resumeAvailable;
	
	//Skills
	
	@FindBy(how = How.ID, using = "btnAddNewSkills")
	@CacheLookup
	WebElement addNewSkills;
	
	
	@FindBy(how = How.CSS, using = "a.js-submit-labels .btn-primary-big .modal-action-button")
	@CacheLookup
	WebElement saveSkillButton;
	
	@FindBy(how = How.CSS, using = "input[placeholder='New skill']")
	@CacheLookup
	WebElement newSkill;
	
	@FindBy(how = How.CSS, using = "a.js-label-submit .btn-action")
	@CacheLookup
	WebElement addButton;
	
	//Jobs
	
	@FindBy(how = How.CSS, using = ".js-modal-add-project .resume-section-button .btn-primary-big")
	@CacheLookup
	WebElement addNewJobs;
	
	@FindBy(how = How.CSS, using = ".js-history-title .field-text")
	@CacheLookup
	WebElement projecttitleText;
	
	@FindBy(how = How.CSS, using = ".js-history-company .field-text")
	@CacheLookup
	WebElement projectcompanyText;
	
	@FindBy(how = How.NAME, using = "historyDescription")
	@CacheLookup
	WebElement DescriptionText;
	
	@FindBy(how = How.ID, using = "historyDateFrom")
	@CacheLookup
	WebElement historyFromDate;
	
	@FindBy(how = How.ID, using = "historyDateTo")
	@CacheLookup
	WebElement historyToDate;
	
	@FindBy(how = How.CSS, using = ".js-date-now-checkbox")
	@CacheLookup
	WebElement untilnowCheckbox;
	
	@FindBy(how = How.CSS, using = ".js-resume-history-submit .btn-primary-big .modal-action-button")
	@CacheLookup
	WebElement saveJobButton;
	
	//Education
	
	@FindBy(how = How.CSS, using = ".js-modal-add-education .resume-section-button .btn-primary-big")
	@CacheLookup
	WebElement addNewEducation;
	
	@FindBy(how = How.CSS, using = ".js-history-title .field-text")
	@CacheLookup
	WebElement InstitutionText;
	
	@FindBy(how = How.CSS, using = ".js-history-description .field-text")
	@CacheLookup
	WebElement fieldofStudyText;
	
	@FindBy(how = How.ID, using = "historyDateFrom")
	@CacheLookup
	WebElement EducationFromDate;
	
	@FindBy(how = How.ID, using = "historyDateTo")
	@CacheLookup
	WebElement EducationToDate;
	
	@FindBy(how = How.CSS, using = ".js-date-now-checkbox")
	@CacheLookup
	WebElement EducationuntilnowCheckbox;
	
	@FindBy(how = How.CSS, using = ".js-resume-history-submit .btn-primary-big .modal-action-button")
	@CacheLookup
	WebElement saveEducationButton;
	
	//certificates
	
	@FindBy(how = How.CSS, using = ".js-modal-add-certificate .resume-section-button .btn-primary-big")
	@CacheLookup
	WebElement addNewCertificates;
	
	@FindBy(how = How.CSS, using = ".js-history-title .field-text")
	@CacheLookup
	WebElement certificateText;
	
	@FindBy(how = How.NAME, using = "historyDescription")
	@CacheLookup
	WebElement certificatesDescriptionText;
	
	@FindBy(how = How.ID, using = "historyDateFrom")
	@CacheLookup
	WebElement certificatesFromDate;
	
	@FindBy(how = How.ID, using = "historyDateTo")
	@CacheLookup
	WebElement certificatesToDate;
	
	@FindBy(how = How.CSS, using = ".js-date-now-checkbox")
	@CacheLookup
	WebElement certificatesuntilnowCheckbox;
	
	@FindBy(how = How.CSS, using = ".js-resume-history-submit .btn-primary-big .modal-action-button")
	@CacheLookup
	WebElement saveCertificateButton;
	
	//Languages
	
	@FindBy(how = How.CSS, using = ".js-modal-add-language .resume-section-button .btn-primary-big")
	@CacheLookup
	WebElement addLanguages;
	
	@FindBy(how = How.CSS, using = ".js-language-selection field-select")
	@CacheLookup
	WebElement languageSelect;
	
	@FindBy(how = How.NAME, using = "js-proficiency-selection field-select")
	@CacheLookup
	WebElement proficiencySelect;
	
	@FindBy(how = How.ID, using = "addNewLanguage")
	@CacheLookup
	WebElement addNewLanguage;
	
	@FindBy(how = How.CSS, using = " .btn-primary-big .modal-action-button")
	@CacheLookup
	WebElement saveLanguageButton;
	
	
	//Software
	
	@FindBy(how = How.ID, using = "btnAddNewSoftware")
	@CacheLookup
	WebElement addSoftware;
	
	@FindBy(how = How.CSS, using = ".js-label-input field-text")
	@CacheLookup
	WebElement softwareText;
	
	@FindBy(how = How.NAME, using = "js-label-submit btn-action")
	@CacheLookup
	WebElement addSoftwareButton;
	
	@FindBy(how = How.CSS, using = " .js-submit-labels btn-primary-big modal-action-button")
	@CacheLookup
	WebElement saveSoftwareButton;
	
	//Industries
	
	@FindBy(how = How.ID, using = "btnAddNewIndustries")
	@CacheLookup
	WebElement addIndustries;
	
	@FindBy(how = How.CSS, using = ".js-label-input field-text")
	@CacheLookup
	WebElement industryText;
	
	@FindBy(how = How.NAME, using = "js-label-submit btn-action")
	@CacheLookup
	WebElement addIndustryButton;
	
	@FindBy(how = How.CSS, using = " .js-submit-labels btn-primary-big modal-action-button")
	@CacheLookup
	WebElement saveIndustryButton;
	
	
	public String getResumeLocation()
	{
		return resumeLocation.getText().trim();
	}
	public String getResumeNationality()
	{
		return resumeNationality.getText().trim();
	}
	public String getResumeAvailability()
	{
		return resumeAvailability.getText().trim();
	}
	public String getResumeMailId()
	{
		return resumeEmail.getText().trim();
	}
	public String getResumePhoneNo()
	{
		return resumePhone.getText().trim();
	}
	public String getResumeName()
	{
		return resumeName.getText().trim();
	}
	public String getResumeAvailable()
	{
		return resumeAvailable.getText().trim();
	}
	
	
	
	public String getProjectTitleError() {
		WebElement ProjectTitleError = driver.findElement(By.id("historyTitle-error"));
		return ProjectTitleError.getText().trim();
	}
	
	public String getProjectDescriptionError() {
		WebElement ProjectDescriptionError = driver.findElement(By.id("historyDescription-error"));
		return ProjectDescriptionError.getText().trim();
	}

	public String getProjectFromDateError() {
		WebElement ProjectFromDateError = driver.findElement(By.id("historyDateFrom-error"));
		return ProjectFromDateError.getText().trim();
	}
	public String getProjectToDateError() {
		WebElement ProjectToDateError = driver.findElement(By.id("historyDateTo-error"));
		return ProjectToDateError.getText().trim();
	}

	public String getInstitutionError() {
		WebElement institutionError = driver.findElement(By.id("historyTitle-error"));
		return institutionError.getText().trim();
	}
	
	public String getFieldofStudyError() {
		WebElement FieldofStudyError = driver.findElement(By.id("historyDescription-error"));
		return FieldofStudyError.getText().trim();
	}

	public String getEducationFromDateError() {
		WebElement educationFromDateError = driver.findElement(By.id("historyDateFrom-error"));
		return educationFromDateError.getText().trim();
	}
	public String getEducationToDateError() {
		WebElement educationToDateError = driver.findElement(By.id("historyDateTo-error"));
		return educationToDateError.getText().trim();
	}
	

	public String getCertificateTitleError() {
		WebElement certificateTitleError = driver.findElement(By.id("historyTitle-error"));
		return certificateTitleError.getText().trim();
	}
	
	public String getcertificateDescriptionError() {
		WebElement FieldofStudyError = driver.findElement(By.id("historyDescription-error"));
		return FieldofStudyError.getText().trim();
	}

	public String getCertificateFromDateError() {
		WebElement certificateFromDateError = driver.findElement(By.id("historyDateFrom-error"));
		return certificateFromDateError.getText().trim();
	}
	public String getCertificateToDateError() {
		WebElement certificateToDateError = driver.findElement(By.id("historyDateTo-error"));
		return certificateToDateError.getText().trim();
	}
	public String getLanguageError() {
		WebElement languageError = driver.findElement(By.id("language-error"));
		return languageError.getText().trim();
	}
	
	public String getLanguageProficiencyError() {
		WebElement languageProficiencyError = driver.findElement(By.id("languageProficiency-error"));
		return languageProficiencyError.getText().trim();
	}
	public void addSkill() {
		addNewSkills.click();
	}
	
	public void setSkill(String skill) {
		newSkill.clear();
		newSkill.sendKeys(skill);
		newSkill.sendKeys(Keys.TAB);
	}
	public void addSkillbutton() {
		addButton.click();
	}
	public void saveSkill() {
		saveSkillButton.click();
	}
	//job
	public void addJob() {
		addNewJobs.click();
	}
	public void setprojectTitle(String title) {
		projecttitleText.clear();
		projecttitleText.sendKeys(title);
		projecttitleText.sendKeys(Keys.TAB);
	}
	public void setprojectCompany(String company) {
		projectcompanyText.clear();
		projectcompanyText.sendKeys(company);
		projectcompanyText.sendKeys(Keys.TAB);
	}
	public void setprojectDescription(String description) {
		DescriptionText.clear();
		DescriptionText.sendKeys(description);
		DescriptionText.sendKeys(Keys.TAB);
	}
	public void setJobFromDate(String fromdate) {
		historyFromDate.clear();
		historyFromDate.sendKeys(fromdate);
		historyFromDate.sendKeys(Keys.TAB);
	}
	public void setJobToDate(String todate) {
		historyToDate.clear();
		historyToDate.sendKeys(todate);
		historyToDate.sendKeys(Keys.TAB);
	}
	public void checkUntillNow() {
		untilnowCheckbox.click();
	}
	public void saveProject() {
		saveJobButton.click();
	}
	//Education
	public void addEducation() {
		addNewEducation.click();
	}
	public void setInstitution(String institute) {
		InstitutionText.clear();
		InstitutionText.sendKeys(institute);
		InstitutionText.sendKeys(Keys.TAB);
	}
	public void setFieldOfStudy(String study) {
		fieldofStudyText.clear();
		fieldofStudyText.sendKeys(study);
		fieldofStudyText.sendKeys(Keys.TAB);
	}
	public void setEducationFromDate(String educationfromdate) {
		EducationFromDate.clear();
		EducationFromDate.sendKeys(educationfromdate);
		EducationFromDate.sendKeys(Keys.TAB);
	}
	public void setEducationToDate(String educationtodate) {
		EducationToDate.clear();
		EducationToDate.sendKeys(educationtodate);
		EducationToDate.sendKeys(Keys.TAB);
	}
	public void saveEducation() {
		saveEducationButton.click();
	}
	
	//Certificates
	public void addCertificates() {
		addNewCertificates.click();
	}
	public void setCertificateTitle(String certificate) {
		certificateText.clear();
		certificateText.sendKeys(certificate);
		certificateText.sendKeys(Keys.TAB);
	}
	public void setCertificateDescription(String cdescription) {
		certificatesDescriptionText.clear();
		certificatesDescriptionText.sendKeys(cdescription);
		certificatesDescriptionText.sendKeys(Keys.TAB);
	}
	
	//Languages
	public void addLanguages() {
		addLanguages.click();
	}
	public void setLanguage(String language) {
		Select selectlanguage = new Select(languageSelect);
		selectlanguage.selectByVisibleText(language);
	}
	public void setLanguageProficiency(String languageproficiency) {
		Select selectproficiency =new Select(proficiencySelect);
		selectproficiency.selectByVisibleText(languageproficiency);
	}
	public void addNewLanguages() {
		addNewLanguage.click();
	}
	public void saveLanguages(){
		saveLanguageButton.click();
	}
	//Software
	public void addSoftwares() {
		addSoftware.click();
	}
	public void setSoftware(String software) {
		softwareText.clear();
		softwareText.sendKeys(software);
		softwareText.sendKeys(Keys.TAB);
	}
	public void addSoftwareButton() {
		addSoftwareButton.click();
	}
	public void saveSoftwareButton() {
		saveSoftwareButton.click();
	}
	
	//Industries
	public void addIndustry() {
		addIndustries.click();
	}
	public void setIndustry(String industry) {
		industryText.clear();
		industryText.sendKeys(industry);
		industryText.sendKeys(Keys.TAB);
	}
	public void addIndustryButton() {
		addIndustryButton.click();
	}
	public void saveIndustryButton() {
		saveIndustryButton.click();
	}
}
