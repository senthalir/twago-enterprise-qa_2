package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ResetPasswordPage {
	protected static WebDriver driver;
	protected static WebDriverWait wait;

	public ResetPasswordPage(WebDriver wd) {
		driver = wd;
		PageFactory.initElements(wd, this);
		wait = new WebDriverWait(wd, 60);
	}

	@FindBy(how = How.ID, using = "password")
	@CacheLookup
	WebElement newPassword;
	
	@FindBy(how = How.ID, using = "confirmPassword")
	@CacheLookup
	WebElement confirmPassword;
	
	@FindBy(how = How.CSS, using = ".account-form-submit[type='submit']")
	@CacheLookup
	WebElement changePassword;
	
	public void setPassword(String password)
	{
		newPassword.clear();
		newPassword.sendKeys(password);
		
	}
	public void setConfirmPassword(String cpassword)
	{
		confirmPassword.clear();
		confirmPassword.sendKeys(cpassword);
		
	}
	 public void clickChangePassword()
	   {
		 changePassword.click();
		 wait.until(ExpectedConditions.titleContains("dashboard"));		
	   }
}
