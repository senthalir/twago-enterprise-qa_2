package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ForgotPasswordPage {
	protected static WebDriver driver;
	protected static WebDriverWait wait;

	public ForgotPasswordPage(WebDriver wd) {
		driver = wd;
		PageFactory.initElements(wd, this);
		wait = new WebDriverWait(wd, 60);
	}

	@FindBy(how = How.NAME, using = "email")
	@CacheLookup
	WebElement eMail;
	
	@FindBy(how = How.CSS, using = ".login-form-submit[type='submit']")
	@CacheLookup
	WebElement requestNewPassword;
	
	public void setEmail(String email)
	{
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("email")));
		eMail.clear();
		eMail.sendKeys(email);
		
	}
	 public void clickRequestNewPassword()
	   {
		 requestNewPassword.click();
		
	   }
}
