package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.BrowserFactory;
import utilities.PropertyHelper;

public class SignupPage {
	protected static WebDriver driver;
	protected static WebDriverWait wait;
	protected static String salutaion;
	protected static String firstName1;
	protected static String lastName1;
	protected static String birthDay1;
	protected static String birthMonth1;
	protected static String birthYear1;
	protected static String correctPhoneNo;
	protected static String correctPassword; 
	public static String emailString;
	
	public SignupPage(WebDriver wd) {
		driver = wd;
		PageFactory.initElements(wd, this);
		wait = new WebDriverWait(wd, 60);
		salutaion = PropertyHelper.GetProperty("salutation");
		firstName1 = PropertyHelper.GetProperty("firstName");
		lastName1 = PropertyHelper.GetProperty("lastName");
		birthDay1 = PropertyHelper.GetProperty("birthDay");
		birthMonth1 = PropertyHelper.GetProperty("birthMonth");
		birthYear1 = PropertyHelper.GetProperty("birthYear");
		correctPhoneNo = PropertyHelper.GetProperty("correctPhoneNo");
		correctPassword = PropertyHelper.GetProperty("correctPassword");
		
	}

	@FindBy(how = How.ID, using = "salutation")
	@CacheLookup
	WebElement salutation;

	@FindBy(how = How.CSS, using = "input[value='m'][type=radio] ~ .radio-text")
	@CacheLookup
	WebElement maleGenderOption;

	@FindBy(how = How.CSS, using = "input[value='f'][type=radio] ~ .radio-text")
	@CacheLookup
	WebElement femaleGenderOption;

	@FindBy(how = How.ID, using = "firstName")
	@CacheLookup
	WebElement firstName;

	@FindBy(how = How.ID, using = "lastName")
	@CacheLookup
	WebElement lastName;

	@FindBy(how = How.ID, using = "email")
	@CacheLookup
	WebElement email;
	
	@FindBy(how = How.ID, using = "birthMonth")
	@CacheLookup
	WebElement birthMonth;

	@FindBy(how = How.ID, using = "birthDaySingle")
	@CacheLookup
	WebElement birthDay;

	@FindBy(how = How.ID, using = "birthYear")
	@CacheLookup
	WebElement birthYear;

	@FindBy(how = How.ID, using = "mobile")
	@CacheLookup
	WebElement mobileNo;

	@FindBy(how = How.ID, using = "phone")
	@CacheLookup
	WebElement phoneNo;

	@FindBy(how = How.ID, using = "password")
	@CacheLookup
	WebElement password;

	@FindBy(how = How.ID, using = "passwordConfirm")
	@CacheLookup
	WebElement passwordConfirm;

	@FindBy(how = How.CSS, using = "#terms ~ .fake-box")
	@CacheLookup
	WebElement termsOption;
	
//	@FindBy(how = How.CLASS_NAME, using = "	fake-box")
//	@CacheLookup
//	WebElement termsOption;


	@FindBy(how = How.CSS, using = "a[href='/terms']")
	@CacheLookup
	WebElement termsLink;

	@FindBy(how = How.CSS, using = "button[type='submit']")
	@CacheLookup
	WebElement signupButton;

	@FindBy(how = How.CSS, using = "a[href='/login']")
	@CacheLookup
	WebElement signinLink;

	public String getfirstNameError() {
		WebElement firstNameError = driver.findElement(By.id("firstName-error"));
		return firstNameError.getText().trim();
	}

	public String getlastNameError() {
		WebElement lastNameError = driver.findElement(By.id("lastName-error"));
		return lastNameError.getText().trim();
	}

	public String getemailError() {
		WebElement emailError = driver.findElement(By.id("email-error"));
		return emailError.getText().trim();
	}

	public String getbirthMonthError() {
		WebElement birthMonthError = driver.findElement(By.id("birthMonth-error"));
		return birthMonthError.getText().trim();
	}

	public String getbirthDaySingleError() {
		WebElement birthDaySingleError = driver.findElement(By.id("birthDaySingle-error"));
		return birthDaySingleError.getText().trim();
	}

	public String getbirthYearError() {
		WebElement birthYearError = driver.findElement(By.id("birthYear-error"));
		return birthYearError.getText().trim();
	}

	public String getmobileError() {
		WebElement mobileError = driver.findElement(By.id("mobile-error"));
		return mobileError.getText().trim();
	}

	public String getphoneError() {
		WebElement phoneError = driver.findElement(By.id("phone-error"));
		return phoneError.getText().trim();
	}

	public String getpasswordError() {
		WebElement passwordError = driver.findElement(By.id("password-error"));
		return passwordError.getText().trim();
	}

	public String getpasswordConfirmError() {
		WebElement passwordConfirmError = driver.findElement(By.id("passwordConfirm-error"));
		return passwordConfirmError.getText().trim();
	}

	public String getpassword() {
		return password.getText().trim();
	}

	public String getpasswordConfirm() {

		return passwordConfirm.getText().trim();
	}



	public void setSalutation(String gender) {
		
		Select selectSalute = new Select(salutation);
		selectSalute.selectByVisibleText(gender);
		
	}

	public void selectFemaleGender() {
		 BrowserFactory.SelectRadioButtonOrCheckbox(femaleGenderOption);
	}

	public void selectMaleGender() {
		 BrowserFactory.SelectRadioButtonOrCheckbox(maleGenderOption);
	}

	public void setFirstName(String firstname) {
		firstName.sendKeys(firstname);
		firstName.sendKeys(Keys.TAB);

	}

	public void setLastName(String lastname) {
		lastName.sendKeys(lastname);
		lastName.sendKeys(Keys.TAB);
	}

	public void setEmail(String email1) {
		email.clear();
		email.sendKeys(email1);

	}

	public void setBirthMonth(String month) {
		Select selectmonth = new Select(birthMonth);
		selectmonth.selectByVisibleText(month);
	}

	public void setBirthDay(String day) {
		Select selectday = new Select(birthDay);
		selectday.selectByVisibleText(day);
	}

	public void setBirthYear(String year) {
		Select selectyear = new Select(birthYear);
		selectyear.selectByVisibleText(year);
	}

	public void setMobileNumber(String mno) {
		mobileNo.clear();
		mobileNo.sendKeys(mno);
	}

	public void setPhoneNumber(String pno) {
		phoneNo.clear();
		phoneNo.sendKeys(pno);
	}

	public void setPassword(String pwd) {
		password.clear();
		password.sendKeys(pwd);
		password.sendKeys(Keys.TAB);
	}

	public void setConfirmPassword(String cPwd) {
		passwordConfirm.clear();
		passwordConfirm.sendKeys(cPwd);
		passwordConfirm.sendKeys(Keys.TAB);
	}

	public void checkTerms() {
		 BrowserFactory.SelectRadioButtonOrCheckbox(termsOption);
	}

	public void clickTerms() {
		termsLink.click();
		wait.until(ExpectedConditions.urlContains("/terms"));
	}

	public void clickSignup() {
		signupButton.click();
		

	}

	public void verifySignup() {
		wait.until(ExpectedConditions.urlContains("/account/created"));
	}

	public void clickSignin() {
		signinLink.click();
		wait.until(ExpectedConditions.urlContains("/login"));
	}

	public void inputValidData() {
		this.setSalutation(salutaion);
		this.selectFemaleGender();
		this.setFirstName(firstName1);
		this.setLastName(lastName1);
		  long rno = Math.round(Math.random()*99999 + 100);
		  emailString ="senthalir.kashme+" +rno+ "@twago.com";
		this.setEmail(emailString);
        this.setBirthDay(birthDay1);
        this.setBirthMonth(birthMonth1);
        this.setBirthYear(birthYear1);
        this.setPhoneNumber(correctPhoneNo);
        this.setMobileNumber(correctPhoneNo);
        this.setPassword(correctPassword);
        this.setConfirmPassword(correctPassword);
        this.checkTerms();
	}

}
