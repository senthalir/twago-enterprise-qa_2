package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
	protected static WebDriver driver;
	protected static WebDriverWait wait;

	public LoginPage(WebDriver wd) {
		driver = wd;
		PageFactory.initElements(wd, this);
		wait = new WebDriverWait(wd, 60);
	}

	@FindBy(how = How.NAME, using = "email")
	@CacheLookup
	WebElement eMail;

	@FindBy(how = How.NAME, using = "password")
	@CacheLookup
	WebElement password;

	@FindBy(how = How.CSS, using = ".login-form-submit[type='submit']")
	@CacheLookup
	WebElement submit;
	
	@FindBy(how = How.CLASS_NAME, using = "login-form-forgot")
	@CacheLookup
	WebElement forgotPassword;

	@FindBy(how = How.CSS, using = "a[href='/signup']")
	@CacheLookup
	WebElement signUp;
	
	
	public void setEmail(String email)
	{
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("email")));
		this.clearEmail();
		eMail.sendKeys(email);
		
	}
	public void clearEmail()
	{
		eMail.clear();
	}
	
	public void setPassword(String pwd)
	{
		this.clearPassword();
		password.sendKeys(pwd);
	}
	public void clearPassword()
	{
		password.clear();
	}
   public void clickSubmit(Boolean isValidCondition)
   {
	   submit.sendKeys(Keys.ENTER);;
	   if (isValidCondition) {
		   wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("a[href='/logout']")));
	   }
   }
   public void clickMspSubmit()
   {
	   submit.click();
	  wait.until(ExpectedConditions.titleContains("Dashboard"));
	  
   }
   public void clickForgotPassword()
   {
	   forgotPassword.click();
   }
   public void clicksignUp()
   {
	   signUp.click();
	   wait.until(ExpectedConditions.urlContains("/signup"));
   }
   
   public String getValidationError() {
	   wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("field-validation-error")));
	   WebElement validationError = driver.findElement(By.className("field-validation-error"));
	   return validationError.getText().trim();
   }
   public String getPasswordError()
   {
	   wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("password-error")));
	   WebElement passwordError =driver.findElement(By.id("password-error"));
	   return passwordError.getText().trim();
	   
   }
	 public String getEmailError()
	 {
		 wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("email-error")));
		 WebElement emailError=driver.findElement(By.id("email-error"));
		 return emailError.getText().trim();
	 }
	   
	   
	   
	   
   }

