package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FreelancerListPage {
	protected static WebDriver driver;
	protected static WebDriverWait wait;

	public FreelancerListPage(WebDriver wd) {
		driver = wd;
		PageFactory.initElements(wd, this);
		wait = new WebDriverWait(wd, 60);
	}


	@FindBy(how = How.CSS, using = "a[href='/freelancer/create']")
	@CacheLookup
	WebElement create;

	 public void clickCreate()
	   {
		//create.click();
		create.sendKeys(Keys.ENTER);
		wait.until(ExpectedConditions.titleContains("Freelancer creation"));
	   }
}
