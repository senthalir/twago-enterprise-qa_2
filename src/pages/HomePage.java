package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
	protected static WebDriver driver;
	protected static WebDriverWait wait;

	public HomePage(WebDriver wd) {
		driver = wd;
		PageFactory.initElements(wd, this);
		wait = new WebDriverWait(wd, 60);
	}

	@FindBy(how = How.CSS, using = ".header-links-item[href='/signup']")
	@CacheLookup
	WebElement signUp;

	@FindBy(how = How.CSS, using = "a[href='/dashboard']")
	@CacheLookup
	WebElement logIn;

	@FindBy(how = How.CSS, using = ".btn-primary[href='/signup']")
	@CacheLookup
	WebElement joinNow;
   public void clickSignUp()
   {
	   signUp.sendKeys(Keys.ENTER);
	   wait.until(ExpectedConditions.urlContains("/signup"));
   }
   public void clicklogIn()
   {
	   logIn.sendKeys(Keys.ENTER);
	   wait.until(ExpectedConditions.urlContains("/login"));
   }
   public void clickJoinNow()
   {
	   joinNow.click();
	   wait.until(ExpectedConditions.urlContains("/signup"));
   }
}
