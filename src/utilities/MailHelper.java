package utilities;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.search.FromTerm;
import javax.mail.search.SubjectTerm;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Properties;

public class MailHelper {
	public static String attachment;
	
	public static String GetUrl(String subject, String urlRegex, String mailId,String password) throws Exception {
		Properties properties = new Properties();
		//props.setProperty("mail.store.protocol", "imaps");
		properties.setProperty("mail.host", "imap.gmail.com");
		properties.setProperty("mail.port", "995");
		properties.setProperty("mail.transport.protocol", "imaps");
		Session session = Session.getDefaultInstance(properties);
		Store store = session.getStore("imaps");
		store.connect("imap.gmail.com", mailId, password);
		Thread.sleep(10000);
		Folder folder = store.getFolder("INBOX");
		folder.open(Folder.READ_WRITE);
		System.out.println("Total Message:" + folder.getMessageCount());
		System.out.println("Unread Message:"
				+ folder.getUnreadMessageCount());
		Message[] messages = null;
		boolean isMailFound = false;
		Message newMail= null;
		//Search for mail from God
		for (int i = 0; i< 5; i++) {
			messages = folder.search(new SubjectTerm(
					subject),
					folder.getMessages());
			//Wait for 10 seconds
			if (messages.length == 0) {
				Thread.sleep(20000);
			}
		}
		//Search for unread mail from God
		//This is to avoid using the mail for which 
		//Registration is already done
		for (Message mail : messages) {
			if (!mail.isSet(Flags.Flag.SEEN)) {
				newMail = mail;
				System.out.println("Message Count is: "
						+ newMail.getMessageNumber());
				isMailFound = true;
			}
		}
		//Test fails if no unread mail was found
		if (!isMailFound) {
			throw new Exception(
					"Could not find new mail");
			//Read the content of mail and launch registration URL                
		} else {
			
			
			String line = newMail.getContent().toString();
			
	        Multipart multipart = (Multipart) newMail.getContent();
			for(int i=0;i<multipart.getCount();i++) {
			    BodyPart bodyPart = multipart.getBodyPart(i);
			    
			    String disposition = bodyPart.getDisposition();

				  if ((disposition != null) && 
				      ((disposition.equals("ATTACHMENT") || 
				       (disposition.equals(Part.INLINE))))) {
				      attachment = bodyPart.getFileName();
				  }
			    
			    if (bodyPart.isMimeType("text/*")) {
			        line = (String) bodyPart.getContent();
			    }
			}
			System.out.println(line);
			//Your logic to split the message and get the Registration URL goes here
			String activationURL = line.split(urlRegex)[1].split("\"")[0].replaceAll("==3D", "=");
				
			System.out.println("URL: "+urlRegex+activationURL);     
			return (activationURL);
		}
	}
}