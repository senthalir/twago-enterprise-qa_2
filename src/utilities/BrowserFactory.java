package utilities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

public class BrowserFactory {
	private static WebDriver wd;
	protected static WebDriverWait wait;

	private static Boolean mspPage;

	public static WebDriver getWebDriver(String browser, String browserDriverUrl, Boolean isMSPPage) throws Exception {
		if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", browserDriverUrl);
			wd = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", browserDriverUrl);
			wd = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", browserDriverUrl);
			wd = new InternetExplorerDriver();
		} else if (browser.equalsIgnoreCase("safari")) {
			wd = new SafariDriver();
		} else if (browser.equalsIgnoreCase("phantom")) {
			wd = new PhantomJSDriver();
		} else {
			throw new Exception("Browser is not correct");
		}
		wait = new WebDriverWait(wd, 60);
		mspPage = isMSPPage;
		return wd;
	}

	public static void LoadWebPage(String subUrl, boolean isInitialLoad) {
		LoadWebPage(subUrl, isInitialLoad, false);
	}
	
	public static void LoadWebPage(String subUrl, boolean isInitialLoad, boolean isMailLink) {
		String baseMSPUrl = PropertyHelper.GetProperty("baseMSPUrl");
		String baseFreelancerUrl = PropertyHelper.GetProperty("baseFreelancerUrl");
		wd.get((isMailLink ? "" : (mspPage ? baseMSPUrl : baseFreelancerUrl)) + subUrl);

		if (isInitialLoad) {
			manageConfirmationPopup();
		}

	}

	public static void SelectRadioButtonOrCheckbox(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
		if (wd instanceof ChromeDriver) {
			Actions actions = new Actions(wd);
			//element.click();
			actions.moveToElement(element).click().perform();
		} else {
			element.click();

		}

	}

	private static void manageConfirmationPopup() {
		try {
			Alert alert = wd.switchTo().alert();
			// alert is present
			System.out.println(alert.getText());
			alert.accept();
			wd.switchTo().defaultContent();
		} catch (NoAlertPresentException n) {
			// Alert isn't present
			return;
		}
	}
}
